#!/bin/sh
#
# This is the pre-commit hook used to prevent
# jshint and jscs error to be commited
#
# This hook will validate jshint and jscs style

# stash unstaged changes
git stash -q --keep-index

# necessary check for initial commit
if git rev-parse --verify HEAD >/dev/null 2>&1; then
    AGAINST_REV=HEAD
else
    # Initial commit: diff against an empty tree object
    AGAINST_REV=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

# Select the files that are Added(A), Copied(C), Modified (M), Renamed (R)
files=$(git diff --cached --name-only --diff-filter=ACMR -- \*.js **/*.js)

pass=true

if [ "$files" != "" ]; then

  for file in ${files}; do

    # Run JSHint validation
    result=$(jshint ${file})
    if [ "$result" != "" ]; then
      for error in "$result"
      do
      echo "${error} \r"
      done
      pass=false
    fi

    # Run JSCS validation
    result=$(jscs ${file})
    if [ "$result" != "No code style errors found." ]; then
      for error in "$result"
      do
      echo "${error} \r"
      done
      pass=false
    fi

  done

fi

# restore unstaged changes
git stash pop -q

if $pass; then
  exit 0
else
  echo ""
  echo "COMMIT FAILED:"
  echo "Some JavaScript files are invalid. Please fix errors and try committing again."
  exit 1
fi

