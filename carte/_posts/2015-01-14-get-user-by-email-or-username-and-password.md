---
category: User
path: '/api/users/login'
title: 'Get a user by the email or username and password (login)'
type: 'GET'

---

This method retrieves a user who exists in the DB

### Request

* The params must include the `email` param (email or username, but the param name is email) to be a valid request.
* The params must include the `password` param to be a valid request.

### Response

* **If succeeds**, returns the user in the DB.
* If the user doesn't exists it must return an error.

```Status: 200 OK```
```{ 
     provider: 'local',
     username: 'yololo',
     email: 'email@email.com',
     __v: 0,
     role: 'user'
}```

For errors responses, see the [response status codes documentation](#response-status-codes).
