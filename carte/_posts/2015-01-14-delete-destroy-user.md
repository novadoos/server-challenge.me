---
category: User
path: '/api/users/:id'
title: 'Destroy a user'
type: 'DELETE'

---

Destroy the user in the DB. (This action can't be undone)

### Request

* **`:id`** is the id the user to destroy.
* **The body is omitted**.

### Response

* If succeed it return a 204 status.

```Status: 204 Deleted```

For errors responses, see the [response status codes documentation](#response-status-codes).
