---
category: User
path: '/api/users'
title: 'Create a user from EMAIL'
type: 'POST'

---

This method is used to create a USER from EMAIL sign-up (simple sign up).

### Request

* **The body can't be empty**

```BODY object```

```{
    username: 'yolo',
    email: 'yolo@email.com',
    password: '12345'
}```

### Response

* **If succeeds**, returns the created user.

```Status: 201 Created```
```{
    id: _id,
    provider: 'local',
    role: 'user',
    username: 'username',
    email: 'user email',
    password: "hashed password"
}```

For errors responses, see the [response status codes documentation](#response-status-codes).
