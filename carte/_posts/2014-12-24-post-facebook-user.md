---
category: User
path: '/api/users/facebook'
title: 'Create a user from FACEBOOK'
type: 'POST'

---

This method is used to create a USER from FACEBOOK sign-up.

### Request

* **The body can't be empty** and the `access_token` attribute must be a valid facebook token.

```BODY object```

```{
    access_token: 'CAAVkFXVLgh0BALt1aB8zpF5wXu0ZCCk...'
}```

### Response

* **If succeeds**, returns the created user.
* If the facebook avatar is not the silhouette will return the avatar; if is the silhouette will return default.


```Status: 201 Created```
```{
    id: _id,
    provider: 'local',
    role: 'user',
    name: "facebook name",
    gender: "facebook gender",
    facebookId: "id from facebook",
    facebookLink: "facebook URL",
    timezone: "facebook timezone",
    locale: "facebook locale",
    password: "hashed password",
    avatar: "facebook avatar"
}```

For errors responses, see the [response status codes documentation](#response-status-codes).
