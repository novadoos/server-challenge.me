---
category: User
path: '/api/users/:id'
title: 'Get a single user by id'
type: 'GET'

---

This method retrieves a single user by the id param.

### Request

* The params must include the `id` param to be a valid request.

### Response

Sends back the appropriate user object

```Status: 200 OK```
```{ 
     provider: 'local',
     username: 'yololo',
     email: 'yolo@email.com',
     __v: 0,
     role: 'user'
}```

For errors responses, see the [response status codes documentation](#response-status-codes).
