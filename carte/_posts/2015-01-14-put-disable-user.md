---
category: User
path: '/api/users/:id'
title: 'Disable a user'
type: 'PUT'

---

this method disable a user when decides to close an account

### Request

* **`:id`** parameter is the id of the user to disable.
* **The body is omitted**.

### Response

* If succeed it return a 201 status.

```Status: 201 UPDATED```

For errors responses, see the [response status codes documentation](#response-status-codes).
