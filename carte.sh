#!/bin/sh
#This may kill the browser
lsof -P | grep ':4000' | awk '{print $2}' | xargs kill -9
#Acces to the folder
cd carte
#Run carte in the browser
jekyll serve --watch & { sleep 2; open http://localhost:4000/; }
