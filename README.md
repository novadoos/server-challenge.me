#### API's Documentation

We've use [carte](https://github.com/devo-ps/carte) to create the documentation, please follow this instructions to visualize all the API's:

1. Install [jekyll](http://jekyllrb.com/docs/installation/) running `$ gem install jekyll` (you need to have [RubyGems](http://rubygems.org/pages/download) installed).
2. Run `$ ./carte.sh` from the root of the project.

It'll show something like this at first:  
![carte first screen](http://devo.ps/images/posts/carte-screenshot.png)
