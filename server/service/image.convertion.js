'use strict';

var gm = require('gm');

exports.image = function(size, from, to, success, error) {
  gm(from)
    .resize(size, size, '^')
    .gravity('Center')
    .crop(size, size)
    .write(to, function(err) {
      if (err) return error(err);
      return success();
    });
};
