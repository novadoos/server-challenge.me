'use strict';

var User = require('../api/user/user.model');

exports.givePoints = function(userID, type, number, callback) {
  if (userID && type && number) {
    User.findOne({_id: userID}, {points:true, totalPoints:true}, function(err, user) {
      if (err) return callback(err);
      if (!user) return callback(new Error('No user found in givePoints'));

      var points = user.points;
      var totalPoints = user.totalPoints ? parseFloat(user.totalPoints) : 0;

      points.push({type:type, amount: number});
      totalPoints += parseFloat(number);

      User.update({_id: userID}, {$set: {points: points, totalPoints: totalPoints}}, function(error, newUser) {
        if (error) return callback(error);
        if (newUser) {
          return callback(null, totalPoints);
        }
      });
    });

  } else {
    return callback(new Error('No data provided in givePoints'));
  }
};
