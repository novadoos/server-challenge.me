'use strict';

var googleapis = require('googleapis');
var youtube = googleapis.youtube('v3');
var OAuth2 = googleapis.auth.OAuth2;
var config = require('../../config/environment');

exports.upload = function(info, stream, callback) {
  //Creates the OAuth2 object
  var oauth2 = new OAuth2(
    config.google.oauth2.clientId,
    config.google.oauth2.secret,
    config.google.oauth2.redirectUri
  );

  //Set credentials
  oauth2.setCredentials(config.google.tokens);

  //Insert video
  //You can get more info on here:
  //Insert info: https://developers.google.com/youtube/v3/docs/videos/insert#parameters
  //Overall info: https://developers.google.com/youtube/v3/docs/videos
  youtube.videos.insert({
    auth: oauth2,
    resource: {
      snippet: info,
      status: {
        privacyStatus: 'private'
      }
    },
    part: 'snippet,status',
    media: {
      body: stream
    }
  }, callback);
};

exports.convertToYoutubeObject = function(input) {
  if (!input) return {};

  var info = {};
  info.title = input.name;
  info.description = input.description;
  info.tags = input.tags;

  return info;
};
