'use strict';

var youtube = require('youtube-api');
var lien = require('lien');
var logger = require('bug-killer');
var readLine = require('readline');
var opn = require('opn');
var config = require('../../config/environment');

// Init lien server
var server = new lien({
  host: 'localhost',
  port: 5000
});

var stdIn = readLine.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Authenticate
// You can access the youtube resources via OAuth2 only.
// https://developers.google.com/youtube/v3/guides/moving_to_oauth#service_accounts
var oauth = youtube.authenticate({
  type: 'oauth',
  client_id: config.google.oauth2.clientId,
  client_secret: config.google.oauth2.secret,
  redirect_url: config.google.oauth2.redirectUri
});

opn(oauth.generateAuthUrl({
  access_type: 'offline',
  approval_prompt: 'force',
  scope: ['https://www.googleapis.com/auth/youtube.upload']
}));

// Handle oauth2 callback
server.page.add('/oauth2callback', function(lien) {
  logger.log('Trying to get the token using the following code: ' + lien.search.code);
  oauth.getToken(lien.search.code, function(err, tokens) {
    if (err) { lien(err, 400); return logger.log(err); }
    oauth.setCredentials(tokens);
    logger.log('These are the tokens: ' + tokens);
  });
});
