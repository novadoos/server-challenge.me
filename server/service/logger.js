'use strict';

var bunyan = require('bunyan');

function reqSerializer(req) {
  return {
    method: req.method,
    url: req.url,
    headers: req.headers
  };
}

module.exports = {
  createLog: function() {
    return bunyan.createLogger({
      name: 'challenge',
      serializers: {req: reqSerializer},
      streams: [
        {path: './logs/logs.log'},
        {stream: process.stdout} // This is for development usage only
      ]
    });
  }
};
