/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /challenges             ->  index
 * POST    /challenges              ->  create
 * GET     /challenges/:id          ->  show
 * PUT     /challenges/:id          ->  update
 * DELETE  /challenges/:id          ->  destroy
 */

'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChallengeSchema = new Schema({
  name: String,
  description: String,
  active: Boolean,
  user: Schema.Types.ObjectId,
  userToShow: String,
  media: String,
  thumbnails: Schema.Types.Mixed,
  volts:[],
  voltsCount: Number,
  tags:[],
  tagsLower:[],
  commentsCount: Number,
  //isPublic:Boolean, //another version
  people:[],
  date: Date,
  daysAvailable: Number,
  shared: Boolean,
  followers: []
});

module.exports = mongoose.model('Challenge', ChallengeSchema);
