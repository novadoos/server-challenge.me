'use strict';

var express = require('express');
var controller = require('./challenge.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/getAllByTag/:tag/:userId/:skip', controller.getChallengesByTag);
router.get('/getAllById/:userId/:skip', controller.getChallengesByUserId);
router.get('/getWorld/:userId/:skip', controller.getWorldChallenges);
router.post('/', controller.create);
router.post('/like', controller.likeChallenge);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
