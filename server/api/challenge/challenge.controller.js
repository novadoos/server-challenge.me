'use strict';

var Challenge = require('./challenge.model');
var User = require('../user/user.model');
var Logger = require('../../service/logger');
var youtube = require('../../service/youtube/video.upload');
var async = require('async');
var logger = Logger.createLog().child({component: 'challenges'});

var validationTrace = function(res, err, message, status) {
  if (!err && message) err = new Error(message);
  if ([500, 408, 400].indexOf(status) > -1) {
    logger.error({err: err}, message);
    if (res && res.json) return res.json(status, err);
  } else if ([200, 201, 204].indexOf(status) > -1) {
    logger.info(message);
  } else if ([401].indexOf(status) > -1) {
    logger.warn(message);
    if (res && res.json) return res.json(status, err);
  }
};

var getChallenges = function(query, skip, sort, userId, callback) {
  var challenges = [];
  var finalChallenges = [];

  async.series({
    searchChallenge: function(cb) {
      //TODO: This is not the best way to paginate. Improve!
      Challenge.find(query, {tags: false}, {skip: skip, limit: 10, sort: sort}, function(err, results) {
        if (err) return cb(err);

        challenges = results;
        cb(null, challenges);
      });
    },
    searchUsers: function(cb) {
      function getUsers(count) {
        var myChallenge = challenges[count].toObject();
        var people = myChallenge.people;

        //Has my like
        myChallenge.hasMyVolt = false;
        if (myChallenge.volts && myChallenge.volts.indexOf(userId) > -1) {
          myChallenge.hasMyVolt = true;
        }

        //Am I following
        myChallenge.following = false;
        if (myChallenge.followers && myChallenge.followers.indexOf(userId) > -1) {
          myChallenge.following = true;
        }

        if (people && people.length > 0) {
          for (var i = 0; i < people.length; i++) {
            if (!people[i]) {
              people.splice(i, 1);
            }
          }

          myChallenge.people = [];
          var usersQuery = {_id: {$in: people}};
          User.find(usersQuery, 'username avatar _id', function(err, userResults) {
            if (err) return cb(err);
            myChallenge.people = userResults;
            finalChallenges.push(myChallenge);

            if (count === challenges.length - 1) {
              cb(null, finalChallenges);
            } else {
              count += 1;
              getUsers(count);
            }
          });
        } else {
          finalChallenges.push(myChallenge);
          if (count === challenges.length - 1) {
            cb(null, finalChallenges);
          } else {
            getUsers(count + 1);
          }
        }
      }

      if (challenges.length > 0) getUsers(0);
    }
  }, function(errors) {
    if (errors) return callback(errors);
    return callback(null, finalChallenges);
  });
};

// Get list of challenges
exports.index = function(req, res) {
  //Challenge.find(function(err, challenges) {
  //  if (err) return validationTrace(res, err, 'Error finding the challenge on index method', 500);
  //
  //  validationTrace(null, null, 'Success on index method', 200);
  //  return res.json(200, challenges);
  //});
  return res.send(200, 'Api not usable');
};

// Get challenges by tag
exports.getChallengesByTag = function(req, res) {
  if (req.params.skip && req.params.tag && req.params.userId) {
    var tag = req.params.tag.toLowerCase();
    var skip = req.params.skip;
    var userId = req.params.userId;
    var findQuery = {
      tagsLower: tag
    };

    getChallenges(findQuery, skip, null, userId, function(err, results) {
      if (err) return validationTrace(res, err, 'Error getting chellenges on getChallengesByTag method', 500);
      if (!results || !results.length)
        return validationTrace(res, null, 'No challenges found on getChallengesByTag method', 500);

      validationTrace(null, null, 'Success on getChallengesByTag method', 200);
      return res.json(200, results);
    });
  } else {
    return validationTrace(res, null, 'No data provided in getChallengesByTag method', 400);
  }
};

// Get challenges by userid
exports.getChallengesByUserId = function(req, res) {
  if (req.params.skip && req.params.userId) {
    var skip = req.params.skip;
    var userId = req.params.userId;
    var findQuery = {
      user: userId
    };

    getChallenges(findQuery, skip, null, userId, function(err, results) {
      if (err) return validationTrace(res, err, 'Error getting chellenges on getChallengesByUserID method', 500);
      if (!results || !results.length)
        return validationTrace(res, null, 'No challenges found on getChallengesByUserID method', 500);

      validationTrace(null, null, 'Success on getChallengesByUserID method', 200);
      return res.json(200, results);
    });
  } else {
    return validationTrace(res, null, 'No data provided in getChallengesByUserID method', 400);
  }
};

// Get Home world challenges
exports.getWorldChallenges = function(req, res) {
  if (req.params.skip && req.params.userId) {
    var skip = req.params.skip;
    var userId = req.params.userId;
    var sortQuery = {
      voltsCount: -1,
      commentsCount: -1
    };

    getChallenges({}, skip, sortQuery, userId, function(err, results) {
      if (err) return validationTrace(res, err, 'Error getting chellenges on getWorldChallenges method', 500);
      if (!results || !results.length)
        return validationTrace(res, null, 'No challenges found on getWorldChallenges method', 404);

      validationTrace(null, null, 'Success on getWorldChallenges method', 200);
      return res.json(200, results);
    });

  } else {
    return validationTrace(res, null, 'No data provided on getWorldChallenges method', 400);
  }
};

// Get Home interests challenges
exports.getInterestsChallenges = function(req, res) {
  if (req.params.skip && req.params.userId) {
    var skip = req.params.skip;
    var userId = req.params.userId;
    var sortQuery = {
      voltsCount: -1,
      commentsCount: -1
    };

    User.findById(userId, 'usedTags', function(error, user) {
      if (error) return validationTrace(res, error, 'Error finding user on getInterestsChallenges method', 500);
      if (!user) return validationTrace(res, null, 'No users found on getInterestsChallenges method', 404);

      if (user.usedTags && user.usedTags.length > 0) {
        var findQuery = {
          usedTags: {
            $eq: user.usedTags
          }
        };

        getChallenges(findQuery, skip, sortQuery, userId, function(err, results) {
          if (err) return validationTrace(res, err, 'Error getting chellenges on getInterestsChallenges method', 500);
          if (!results || !results.length)
            return validationTrace(res, null, 'No challenges found on getInterestsChallenges', 404);

          validationTrace(null, null, 'Success on getInterestsChallenges method', 200);
          return res.json(200, results);
        });
      } else {
        validationTrace(null, null, 'Success on getInterestsChallenges method', 200);
        return res.json(200, []);
      }
    });

  } else {
    return validationTrace(res, null, 'No data provided in getInterestsChallenges method', 500);
  }
};

// Get a single challenge
exports.show = function(req, res) {
  Challenge.findById(req.params.id, function(err, challenge) {
    if (err) return validationTrace(res, err, 'Error finding challenge on show method', 500);
    if (!challenge) return validationTrace(res, null, 'No challenge found on show method', 404);

    validationTrace(null, null, 'Success on show method', 200);
    return res.json(200, challenge);
  });
};

// Creates a new challenge in the DB.
exports.create = function(req, res) {
  if (req.busboy) {
    var challenge;

    async.series({
        createBaseChallenge: function(callback) {
          req.busboy.on('field', function(fieldname, val) {
            req.body[fieldname] = val;
          });

          req.busboy.on('file', function(fieldname, file) {
            challenge = new Challenge(req.body);

            if (challenge &&
              challenge.name &&
              challenge.description &&
              challenge.daysAvailable) {

              challenge.date = new Date();
              challenge.active = true;

              if (challenge.tags) {
                challenge.tags = challenge.tags.toString().replace(/['"\[\]]/g, '').split(',');
                var lowerTags = [];
                for (var i = 0; i < challenge.tags.length; i++) {
                  lowerTags.push(challenge.tags[i].toLowerCase());
                }
                challenge.tagsLower = lowerTags;
              }

              if (challenge.people) {
                challenge.people = challenge.people.toString().replace(/['"\[\]]/g, '').split(',');
                if (challenge.people.length > 0) {
                  for (var i = 0; i < challenge.people.length; i++) {
                    if (!challenge.people[i]) {
                      challenge.people.splice(i, 1);
                    }
                  }
                }
              }
            } else {
              return callback(new Error('no data provided'));
            }

            var videoInfo = youtube.convertToYoutubeObject(challenge);
            youtube.upload(videoInfo, file, function(err, data) {
              if (err) return callback(err);
              challenge.media = data.id;
              challenge.thumbnails = data.snippet && data.snippet.thumbnails ? data.snippet.thumbnails : {};
              callback(null, challenge);
            });
          });

          req.pipe(req.busboy);
        },
        saveChallenge: function(callback) {
          var userId = challenge.user;
          User.findById(userId, 'usedTags username name', function(error, user) {
            if (error) return callback(error);
            if (!user) return callback(new Error('No user found in create challenge'));

            var usedTags = user.usedTags;
            var lowerTags = challenge.tagsLower;
            for (var i = 0; i < lowerTags.length; i++) {
              if (usedTags.indexOf(lowerTags[i]) === -1) {
                usedTags.push(lowerTags[i]);
              }
            }
            User.update({_id: userId}, {$set: {usedTags: usedTags}}, function(updateError, newUser) {
              if (updateError) return callback(updateError);
              if (!newUser) return callback(new Error('Couldn\'t update user'));

              challenge.userToShow = user.username ? user.username : user.name;

              challenge.save(function(err, challengeSaved) {
                if (err) return callback(err);
                if (!challengeSaved) return callback(new Error('No challenge saved'));

                challenge = challengeSaved;
                callback(null, challenge);
              });
            });
          });
        }
      },
      function(err, results) {
        function deleteChallenge(error) {
          if (challenge && challenge._id) {
            Challenge.findByIdAndRemove(challenge._id, function(removeErr) {
              if (removeErr) return validationTrace(res, removeErr, 'Error deleting a challenge on create method', 500);
              return validationTrace(res, error, 'Errors creating a challenge on create method', 500);
            });
          } else {
            return validationTrace(res, error, 'Errors creating a challenge on create method', 500);
          }
        }

        if (err) deleteChallenge(err);
        if (!results) deleteChallenge('No challenge saved');

        validationTrace(null, null, 'Success on create method', 200);
        return res.json(200, results.saveChallenge);
      });

  } else {
    return validationTrace(res, null, 'No busboy provided?', 500);
  }
};

// Like and unlike challenge
exports.likeChallenge = function(req, res) {
  if (req.body.challengeId && req.body.userId) {
    var challengeId = req.body.challengeId;
    var userId = req.body.userId;

    try {
      Challenge.findOne({_id: challengeId}, {_id:false, volts:true, voltsCount:true}, function(err, challenge) {
        if (err) return validationTrace(res, err, 'Error finding challenge on likeChallenge method', 500);
        if (!challenge) return validationTrace(res, null, 'No challenge returned in likeChallenge method', 500);

        var volts = challenge.volts ? challenge.volts : [];
        var voltsCounter = challenge.voltsCount ? challenge.voltsCount : 0;

        if (volts.indexOf(userId) > -1) {
          volts.splice(volts.indexOf(userId), 1);
        } else {
          volts.push(userId);
        }

        voltsCounter = volts.length;

        Challenge.update({_id: challengeId}, {$set: {volts: volts, voltsCount: voltsCounter}}, function(error, updChallenge) {
          if (err) return validationTrace(res, error, 'Error updating the challenge on likeChallenge method', 500);
          if (updChallenge) {
            validationTrace(null, null, 'Success on likeChallenge method', 200);
            return res.json(200, {id: challengeId, number: volts.length});
          }
        });
      });
    } catch (catchError) {
      return validationTrace(res, catchError, 'Error on likeChallenge method', 500);
    }
  } else {
    return validationTrace(res, null, 'No attributes were given in like challenge', 400);
  }
};

// Updates an existing challenge in the DB.
exports.update = function(req, res) {
  //if (req.body._id) { delete req.body._id; }
  //Challenge.findById(req.params.id, function(err, challenge) {
  //  if (err) return validationTrace(res, err, 'Error finding a challenge on update method', 500);
  //  if (!challenge) return validationTrace(res, null, 'No challenge found on update method', 404);
  //
  //  var updated = _.merge(challenge, req.body);
  //  updated.save(function(err) {
  //    if (err) return validationTrace(res, err, 'Error saving the challenge on update method', 500);
  //
  //    validationTrace(null, null, 'Success on update method', 200);
  //    return res.json(200, challenge);
  //  });
  //});
  return res.send(200, 'Api not usable');
};

// Deletes a challenge from the DB.
exports.destroy = function(req, res) {
  //Challenge.findById(req.params.id, function(err, challenge) {
  //  if (err) return validationTrace(res, err, 'Error finding a challenge on destroy method', 500);
  //  if (!challenge) return validationTrace(res, null, 'No challenge found on destroy method', 404);
  //
  //  challenge.remove(function(err) {
  //    if (err) return validationTrace(res, err, 'Error removing challenge on destroy method', 500);
  //
  //    validationTrace(null, null, 'Success on destroy method', 200);
  //    return res.send(204);
  //  });
  //});
  return res.send(200, 'Api not usable');
};
