'use strict';

var _ = require('lodash');
var Challenge = require('../challenge/challenge.model');
var User = require('../user/user.model');
var Logger = require('../../service/logger');
var logger = Logger.createLog().child({component: 'users'});
var async = require('async');

var validationTrace = function(res, err, message, status) {
  if (!err && message) err = new Error(message);
  if ([500, 408, 400].indexOf(status) > -1) {
    logger.error({err: err}, message);
    if (res && res.json) return res.json(status, err);
  } else if ([200, 201, 204].indexOf(status) > -1) {
    logger.info(message);
  } else if ([401].indexOf(status) > -1) {
    logger.warn(message);
    if (res && res.json) return res.json(status, err);
  }
};

exports.index = function(req, res) {
  if (!req.params.search || !req.params.skip || !req.params.username)
    return validationTrace(res, null, 'No data provided in the index method', 400);

  var params = req.params.search;
  var myUsername = req.params.username;
  var elementsLimit = 25;
  var elements = [];

  async.parallel({
      users: function(callback) {
        var findQuery = {
          username: {
            $in: [new RegExp(params, 'g')],
            $ne: myUsername
          }
        };

        User.find(findQuery, {}, {skip: req.params.skip, limit: elementsLimit}, function(err, results) {
          if (err) return callback(err);
          for (var i = 0; i < results.length; i++) {
            var user = results[i];
            elements.push({
              type: 'user',
              image: user.avatar,
              id: user._id
            });
          }

          callback(null, elements);
        });

      },
      challenges: function(callback) {
        var findQuery = {
          name: {
            $in: [new RegExp(params, 'g')]
          }
        };

        Challenge.find(findQuery, {}, {skip: req.params.skip, limit: elementsLimit}, function(err, results) {
          if (err) return callback(err);
          for (var i = 0; i < results.length; i++) {
            var challenge = results[i];
            elements.push({
              type: 'challenge',
              image: challenge.thumbnail ? challenge.thumbnail : challenge.media,
              id: challenge._id
            });
          }

          callback(null, elements);
        });
      }
    },
    function(errors) {
      if (errors) return validationTrace(res, errors, 'Errors on index method', 500);

      validationTrace(null, null, 'Success on index method', 200);
      return res.json(200, elements);
    }
  )
};
