'use strict';

var _ = require('lodash');
var Notification = require('./notification.model');
var Logger = require('../../service/logger');
var logger = Logger.createLog().child({component: 'comments'});

var validationError = function(res, err, status) {
  logger.error({err: err}, err.toString());
  if (res && res.json) return res.json(status, err);
  return;
};

// Get list of notifications
exports.index = function(req, res) {
  Notification.find(function(err, notifications) {
    if (err) { return validationError(res, err, 500); }
    return res.json(200, notifications);
  });
};

// Get a single notification
exports.show = function(req, res) {
  Notification.findById(req.params.id, function(err, notification) {
    if (err) return validationError(res, err, 500);
    if (!notification) return validationError(res, new Error('No notification found on show'), 404);
    return res.json(notification);
  });
};

// Creates a new notification in the DB.
exports.create = function(req, res) {
  Notification.create(req.body, function(err, notification) {
    if (err) { return validationError(res, err, 500); }
    return res.json(201, notification);
  });
};

// Updates an existing notification in the DB.
exports.update = function(req, res) {
  if (req.body._id) { delete req.body._id; }
  Notification.findById(req.params.id, function(err, notification) {
    if (err) { return validationError(res, err, 500); }
    if (!notification) return validationError(res, new Error('No notification found on update'), 404);
    var updated = _.merge(notification, req.body);
    updated.save(function(err) {
      if (err) { return validationError(res, err, 500); }
      return res.json(200, notification);
    });
  });
};

// Deletes a notification from the DB.
exports.destroy = function(req, res) {
  Notification.findById(req.params.id, function(err, notification) {
    if (err) { return validationError(res, err, 500); }
    if (!notification) return validationError(res, new Error('No notification found on destroy'), 404);
    notification.remove(function(err) {
      if (err) { return validationError(res, err, 500); }
      return res.send(204);
    });
  });
};
