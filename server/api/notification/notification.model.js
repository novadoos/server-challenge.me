'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationSchema = new Schema({
  name: String,
  user: Schema.Types.ObjectId,
  description: String,
  type: String,
  thumbnail: String,
  link: String,
  active: Boolean,
  created: Date
});

module.exports = mongoose.model('Notification', NotificationSchema);
