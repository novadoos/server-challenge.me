/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var User = require('./user.model');
var mainSocket = null;

function onSave(socket, doc, cb) {
  socket.emit('user:save', doc);
}
function onRemove(socket, doc, cb) {
  socket.emit('user:remove', doc);
}

exports.register = function(socket, socketio) {
  mainSocket = socketio;
  User.schema.post('save', function(doc) {
    onSave(socket, doc);
  });
  User.schema.post('remove', function(doc) {
    onRemove(socket, doc);
  });
};

exports.getUsersByUsername = function(socket, doc) {
  mainSocket.to(socket).emit('user:getUsersByUsername', doc);
};
