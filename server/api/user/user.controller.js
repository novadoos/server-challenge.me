'use strict';

var User = require('./user.model');
var userSocket = require('./user.socket');
var Challenge = require('../challenge/challenge.model');
var Logger = require('../../service/logger');
var FB = require('fb');
var request = require('request');
var fs = require('fs');
var path = require('path');
var async = require('async');
var config = require('../../config/environment');
var image = require('../../service/image.convertion');
var lib = require('./user.lib.js');
var email = require('./user.email');
var logger = Logger.createLog().child({component: 'users'});

var validationTrace = function(res, err, message, status) {
  if (!err && message) err = new Error(message);
  if ([500, 408, 400].indexOf(status) > -1) {
    logger.error({err: err}, message);
    if (res && res.json) return res.json(status, err);
  } else if ([200, 201, 204].indexOf(status) > -1) {
    logger.info(message);
  } else if ([401].indexOf(status) > -1) {
    logger.warn(message);
    if (res && res.json) return res.json(status, err);
  }
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  //User.find({}, '-salt -hashedPassword', function(err, users) {
  //  if (err) validationTrace(res, err, 'There is a problem in the index method', 500);
  //
  //  validationTrace(null, null, 'Success on index method', 200);
  //  res.json(200, users);
  //});
  return res.send(200, 'Api not usable');
};

/**
 * Get list of users by username
 */
exports.getUsersByUsername = function(req, res) {
  if (req.params.username && req.params.myUsername) {
    var username = req.params.username.toLowerCase();
    var myUsername = req.params.myUsername.toLowerCase();

    var query = {
      username: {
        $in: [new RegExp(username, 'g')],
        $ne: myUsername
      }
    };

    User.find(query, '_id username avatar', {limit: 50}, function(err, users) {
      if (err) return validationTrace(res, err, 'There is a problem in the getUsersByUsername method', 500);

      validationTrace(null, null, 'Success on getUsersByUsername method', 200);
      return res.json(200, users);
    });
  } else {
    return validationTrace(res, null, 'No data provided in the getUsersByUsername method', 400);
  }
};

/**
 * Creates a new user from EMAIL
 */
exports.create = function(req, res) {
  var newUser = new User(req.body);
  newUser.provider = config.providers.local;
  newUser.role = config.roles.user;
  newUser.enabled = true;

  newUser.save(function(err, userSaved) {
    if (err) return validationTrace(res, err, 'Couldn\'t create the user on create method', 500);
    if (!userSaved) return validationTrace(res, null, 'No user created on create method', 500);

    email.welcome(newUser, function(error) {
      if (error) return validationTrace(res, error, 500);

      validationTrace(null, null, 'Success creating the user on create method', 200);
      return res.json(201, userSaved);
    });
  });
};

/**
 * Creates a new user from FACEBOOK
 */
exports.createFromFacebook = function(req, response) {
  FB.setAccessToken(req.body.access_token);

  FB.api('/me', function(res) {
    if (!res.email) {
      FB.api('me/permissions', 'DELETE', function(res) {
        if (res.error) return validationTrace(response, res.error, 'There is a problem deleting facebook permissions on createFromFacebook method', 500);
        return validationTrace(response, null, 'No facebook email permissions on createFromFacebook method', 500);
      });
    } else {
      if (res && !res.error) {
        if (res.id) {
          User.findOne({facebookId: res.id}, '-salt -hashedPassword', function(err, user) {
            if (err) return validationTrace(response, err, 'There is a problem with User.findOne on createFromFacebook method', 500);
            if (user) {
              FB.api('/me',
                {access_token: user.facebookLToken},
                function(res) {
                  if (!res)
                    return validationTrace(response, null, 'No facebook response on method createFromFacebook', 500);
                  if (res.error) {
                    validationTrace(null, res.error, 'Facebook error on createFromFacebook method', 500);

                    FB.api('oauth/access_token', {
                        client_id: config.facebook.clientID,
                        client_secret: config.facebook.clientSecret,
                        grant_type: 'fb_exchange_token',
                        fb_exchange_token: req.body.access_token
                      },
                      function(ftRes) {
                        if (!ftRes)
                          return validationTrace(response, null, 'No facebook response for ouath/accessToken on method createFromFacebook', 500);
                        if (ftRes.error)
                          return validationTrace(response, ftRes.error, 'Facebook error for ouath/accessToken on createFromFacebook method', 500);

                        user.facebookLToken = ftRes.access_token;

                        validationTrace(null, null, 'Success on createFromFacebook method', 200);
                        return response.json(200, user);
                      });
                  } else {
                    validationTrace(null, null, 'Success on createFromFacebook method', 200);
                    return response.json(200, user);
                  }
                }
              );
            } else {
              var newUser = new User();
              async.series({
                //TODO: Get never expires token
                createFacebookLongToken: function(callback) {
                  FB.api('oauth/access_token', {
                      client_id: config.facebook.clientID,
                      client_secret: config.facebook.clientSecret,
                      grant_type: 'fb_exchange_token',
                      fb_exchange_token: req.body.access_token
                    },
                    function(ftRes) {
                      if (!ftRes)
                        return callback(new Error('No facebook response for ouath/accessToken on method createFromFacebook'));
                      if (ftRes.error)
                        return callback(ftRes.error);

                      newUser.facebookLToken = ftRes.access_token;
                      callback(null, newUser);
                    });
                },
                createBaseUser: function(callback) {
                  newUser.provider = config.providers.local;
                  newUser.role = config.roles.user;
                  newUser.name = res.name ? res.name : '';
                  newUser.gender = res.gender ? res.gender : '';
                  newUser.facebookId = res.id ? res.id : '';
                  newUser.facebookLink = res.link ? res.link : '';
                  newUser.timezone = res.timezone;
                  newUser.locale = res.locale ? res.locale : '';
                  newUser.password = lib.generatePassword();
                  newUser.email = res.email;
                  newUser.enabled = true;

                  newUser.save(function(err, userSaved) {
                    if (err) return callback(err);
                    if (!userSaved) return callback(new Error('no user saved on createBaseUser'));
                    callback(null, userSaved);
                  });
                },
                setUserAvatar: function(callback) {
                  FB.api('/me/picture',
                    {
                      redirect: false,
                      height: config.avatarHeight,
                      type: 'normal',
                      width: config.avatarWidth
                    },
                    function(res) {
                      if (res && !res.error) {
                        if (res.data) {
                          //jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                          if (res.data.is_silhouette) {
                            //jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                            newUser.avatar = path.join
                            (
                              config.root + config.avatarFolder +
                              newUser.defaultAvatarName + config.imageFormat
                            );
                            callback(null, newUser);
                          } else {
                            request.get(res.data.url)
                              .on('error', function(err) {
                                return callback(err);
                              })
                              .on('response', function() {
                                newUser.avatar = path.join
                                (
                                  config.root + config.avatarFolder +
                                  newUser._id + config.imageFormat
                                );
                                callback(null, newUser);
                              })
                              .pipe(
                              fs.createWriteStream(
                                path.join(config.root + config.avatarFolder) +
                                newUser._id + config.imageFormat)
                            );
                          }
                        } else {
                          return callback(new Error('No data were given'));
                        }
                      } else {
                        if (res.error.code === config.errorCodes.ETIMEDOUT) {
                          return callback(res.error);
                        } else {
                          return callback(res.error);
                        }
                      }
                    }
                  );
                },
                updateAvatarAndSendEmail: function(callback) {
                  var userToUpdate = newUser.toObject();
                  User.update({username: newUser.username}, userToUpdate, function(err, userSaved) {
                    if (err) return callback(err);
                    if (!userSaved) return callback(new Error('No user saved on updateAvatarAndSendEmail'));

                    email.welcome(newUser, function(error) {
                      if (error) return callback(error);
                      callback(null, newUser);
                    });
                  });
                }
              }, function(errors, results) {
                function deleteUser(error) {
                  if (newUser && newUser._id) {
                    var userToRemove = newUser;
                    User.findByIdAndRemove(newUser._id, function(removeErr) {
                      if (fs.existsSync(userToRemove.avatar)) fs.unlinkSync(userToRemove.avatar);
                      if (removeErr) return validationTrace(response, removeErr, 'Error deleting user on createFromFacebook method', 500);
                      return validationTrace(response, error, 'Errors on createFromFacebook method', 500);
                    });
                  } else {
                    return validationTrace(response, error, 'Errors on createFromFacebook method', 500);
                  }
                }

                if (errors) deleteUser(errors);
                if (!results) deleteUser('No user saved on final function async');

                validationTrace(null, null, 'Success on createFromFacebook method', 201);
                return response.json(201, results.updateAvatarAndSendEmail);
              });
            }
          });
        }
      } else {
        if (res.error) {
          if (res.error.code === config.errorCodes.ETIMEDOUT) {
            return validationTrace(response, res.error, 'ETIMEDOUT on createFromFacebook method', 408);
          } else {
            return validationTrace(response, res.error, 'Error for facebook response on createFromFacebook method', 500);
          }
        }
      }
    }
  });
};

/**
 * Validates a facebook token
 */
exports.validateFacebookToken = function(req, response) {
  if (!req.params.facebookToken) return validationTrace(response, null, 'Bad request, facebook token not sent on validateFacebookToken method', 400);
  var fbToken = req.params.facebookToken;

  FB.api('/me',
    {access_token: fbToken},
    function(res) {
      if (!res)
        return validationTrace(response, null, 'No facebook response on validateFacebookToken method', 500);
      if (res.error)
        return validationTrace(response, res.error, 'Error from facbook on validateFacebookToken method', 500);

      validationTrace(null, null, 'Success on validateFacebookToken method', 200);
      response.json(200, true);
    }
  );
};

/**
 * Renew a facebook long term token
 */
exports.renewToken = function(req, response) {
  if (req.body.access_token && req.body.userId) {
    FB.setAccessToken(req.body.access_token);

    FB.api('oauth/access_token', {
        client_id: config.facebook.clientID,
        client_secret: config.facebook.clientSecret,
        grant_type: 'fb_exchange_token',
        fb_exchange_token: req.body.access_token
      },
      function(ftRes) {
        if (!ftRes)
          return validationTrace(response, null, 'No facebook response on validateFacebookToken method', 500);
        if (ftRes.error)
          return validationTrace(response, ftRes.error, 'Error from facbook on validateFacebookToken method', 500);

        var userId = req.body.userId;

        User.update({_id:userId}, {$set: {facebookLToken: ftRes.access_token}}, function(err, result) {
          if (err) return validationTrace(response, err, 'Error updating the user on renewToken method', 500);

          validationTrace(null, null, 'Success on renewToken method', 200);
          if (result) return response.json(200, ftRes.access_token);
        });
      });

  } else {
    return validationTrace(response, new Error('No token provided in renewToken'), 400);
  }
};

/**
 * Get a single user by id
 */
exports.show = function(req, res) {
  if (!req.params.id) return validationTrace(res, null, 'Bad request id not sent on show method', 400);

  //Sometimes the user is 'me'
  var userId = req.params.id && parseFloat(req.params.id) ? req.params.id : 0;
  if (!userId) return validationTrace(res, null, 'No valid user ID on show method', 400);

  User.findOne({
    _id: userId
  }, '-salt -hashedPassword -_id', function(err, user) {
    if (err) return validationTrace(res, err, 'Error finding the User on show method', 500);
    if (!user) return validationTrace(res, null, 'No user found on user by id', 404);

    validationTrace(null, null, 'Success on show method', 200);
    return res.json(200, user);
  });
};

/**
 * Get a single user by email/username and password
 */
exports.login = function(req, res) {
  var emailORUsername = req.params.email;
  var password = req.params.password;

  if (!emailORUsername || !password)
    return validationTrace(res, null, 'Email or password empty', 400);

  User.findOne({
    $or: [
      {email: emailORUsername},
      {username: emailORUsername}
    ]
  }, '-_id', function(err, user) {
    if (err) return validationTrace(res, err, 'Error finding the User on login method', 500);
    if (!user) return validationTrace(res, null, 'No user found on login method', 401);

    password = lib.encryptPassword(user.salt, password);

    User.findOne({
      $or: [
        {email: emailORUsername},
        {username: emailORUsername}
      ],
      hashedPassword: password
    }, '-salt -hashedPassword', function(error, finalUser) {
      if (error) return validationTrace(res, error, 'Error finding the User with salt on login method', 500);
      if (!finalUser) return validationTrace(res, null, 'No user found on login method', 401);

      validationTrace(null, null, 'Success on login method', 200);
      return res.json(200, finalUser);
    });
  });
};

/**
 * Follow a user
 */
exports.setUserFollower = function(req, res) {
  if (req.body && req.body.id && req.body.followToId) {
    var userId = req.body.id;
    var userToFollow = req.body.followToId;

    async.parallel([
        function(callback) {
          User.findById(userId, 'following', function(err, user) {
            if (err) return callback(err);
            if (!user) return callback(new Error('No user found on follow'));

            var following = user.following;
            if (following.indexOf(userToFollow) > -1) {
              following.splice(following.indexOf(userToFollow), 1);
            } else {
              following.push(userToFollow);
            }

            User.update({'_id': userId}, {$set: {'following': following}}, function(updateErr, newUser) {
              if (updateErr) return callback(updateErr);
              if (!newUser) return callback(new Error('No user found on follow'));

              callback(null, newUser);
            });
          });
        },
        function(callback) {
          User.findById(userToFollow, 'followers', function(err, user) {
            if (err) return callback(err);
            if (!user) return callback(new Error('No user found on follow'));

            var followers = user.followers;
            if (followers.indexOf(userId) > -1) {
              followers.splice(followers.indexOf(userId), 1);
            } else {
              followers.push(userId);
            }

            User.update({'_id': userId}, {$set: {'followers': followers}}, function(updateErr, newUser) {
              if (updateErr) return callback(updateErr);
              if (!newUser) return callback(new Error('No user found on follow'));

              callback(null, newUser);
            });
          });
        }
      ],
      function(err, results) {
        if (err) return validationTrace(res, err, 'Errors on setUserFollower method', 500);

        validationTrace(null, null, 'Success on setUserFollower method', 200);
        res.json(200, results);
      });
  } else {
    return validationTrace(res, null, 'No attributes were given in setUserFollower', 400);
  }
};

/**
 * Follow a challenge
 */
exports.setChallengeFollower = function(req, res) {
  if (req.body && req.body.id && req.body.followToId) {
    var userId = req.body.id;
    var challengeToFollow = req.body.followToId;
    var usedTags = [];
    var followStatus = false;

    async.series([
        function(callback) {
          User.findById(userId, 'followingChallenges usedTags', function(err, user) {
            if (err) return callback(err);
            if (!user) return callback(new Error('No user found on follow'));

            usedTags = user.usedTags;

            var followingChallenges = user.followingChallenges;
            if (followingChallenges.indexOf(challengeToFollow) > -1) {
              followingChallenges.splice(followingChallenges.indexOf(challengeToFollow), 1);
            } else {
              followStatus = true;
              followingChallenges.push(challengeToFollow);
            }

            User.update({'_id': userId}, {$set: {'followingChallenges': followingChallenges}}, function(updateErr, newUser) {
              if (updateErr) return callback(updateErr);
              if (!newUser) return callback(new Error('No user found on follow'));

              callback(null, newUser);
            });
          });
        },
        function(callback) {
          Challenge.findById(challengeToFollow, 'followers tagsLower', function(err, challenge) {
            if (err) return callback(err);
            if (!challenge) return callback(new Error('No challenge found on follow'));

            var lowerTags = challenge.tagsLower ? challenge.tagsLower : [];

            var followers = challenge.followers;
            if (followers.indexOf(userId) > -1) {
              followers.splice(followers.indexOf(userId), 1);
            } else {
              followers.push(userId);
            }

            Challenge.update({'_id': challengeToFollow}, {$set: {'followers': followers}}, function(updateErr, newChallenge) {
              if (updateErr) return callback(updateErr);
              if (!newChallenge) return callback(new Error('No challenge found on follow'));

              if (followStatus === true) {
                for (var i = 0; i < lowerTags.length; i++) {
                  if (usedTags.indexOf(lowerTags[i]) === -1) {
                    usedTags.push(lowerTags[i]);
                  }
                }

                User.update({_id: userId}, {$set: {usedTags: usedTags}}, function(userErr, newUser) {
                  if (userErr) return callback(userErr);
                  if (!newUser) return callback(new Error('No user found on follow'));

                  callback(null, newUser);
                });
              } else {
                callback(null, newChallenge);
              }
            });
          });
        }
      ],
      function(err, results) {
        if (err) return validationTrace(res, err, 'Errors on setChallengeFollower method', 500);

        validationTrace(null, null, 'Success on setChallengeFollower method', 200);
        res.json(200, results);
      });
  } else {
    return validationTrace(res, null, 'No attributes were given in setChallengeFollower', 400);
  }
};

/**
 * Get the 'followers' users
 */
exports.getFollowers = function(req, res) {
  if (req.params.id) {
    var userId = req.params.id;

    User.findById(userId, function(err, user) {
      if (err) return validationTrace(res, err, 'Error finding the user on getFollowers method', 500);
      if (!user) return validationTrace(res, null, 'No user found on getFollowers method', 404);

      validationTrace(null, null, 'Success on getFollowers method', 200);
      res.json(200, user.followers);
    });
  } else {
    return validationTrace(res, null, 'No attributes were given in getFollowers', 400);
  }
};

/**
 * Get the 'following' users
 */
exports.getFollowingUsers = function(req, res) {
  if (req.params.id) {
    var userId = req.params.id;

    User.findById(userId, function(err, user) {
      if (err) return validationTrace(res, err, 'Error finding the user on getFollowingUsers method', 500);
      if (!user) return validationTrace(res, null, 'No user found on getFollowingUsers method', 404);

      validationTrace(null, null, 'Success on getFollowingUsers method', 200);
      res.json(200, user.following);
    });
  } else {
    return validationTrace(res, null, 'No attributes were given in getFollowingUsers method', 400);
  }
};

/**
 * Get the 'following' challenges
 */
exports.getFollowingChallenges = function(req, res) {
  if (req.params.id) {
    var userId = req.params.id;

    User.findById(userId, function(err, user) {
      if (err) return validationTrace(res, err, 'Error finding the user on getFollowingChallenges method', 500);
      if (!user) return validationTrace(res, null, 'No user found on getFollowingChallenges method', 404);

      validationTrace(null, null, 'Success on getFollowingChallenges method', 200);
      res.json(200, user.followingChallenges);
    });
  } else {
    return validationTrace(res, null, 'No attributes were given in getFollowingChallenges', 400);
  }
};

/**
 * Destroys a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  //TODO: destroy everything
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if (err) return validationTrace(res, err, 'Error finding the user on destroy method', 500);
    email.destroy(user, function(error) {
      if (error) return validationTrace(res, error, 'Error sending email on destroy method', 500);

      validationTrace(null, null, 'Success on destroy method', 204);
      return res.send(204);
    });
  });
};

/**
 * 'Disable' a user
 */

exports.disable = function(req, res) {
  var userId = req.user._id;

  User.findById(userId, function(err, user) {
    user.enabled = false;
    user.save(function(err) {
      if (err) return validationTrace(res, err, 'Error finding the user on disable method', 500);
      email.disable(user, function(error) {
        if (error) return validationTrace(res, error, 'Error sending email on disable method', 500);

        validationTrace(null, null, 'Success on disable method', 201);
        return res.send(201);
      });
    });
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function(err, user) {
    if (user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationTrace(res, err, 'Error finding the user on changePassword method', 500);

        validationTrace(null, null, 'Success on changePassword method', 200);
        res.send(200);
      });
    } else {
      return validationTrace(res, 'Old password on changePassword method', 403);
    }
  });
};

/**
 * Avatar image upload
 */
exports.avatar = function(req, res) {
  if (!req || !req.body || !req.body.imgdata || !req.body.userId)
    return validationTrace(res, null, 'an user ID and image must be sent on avatar method', 400);

  var tempUrl = path.join(config.root, config.tempFolder);
  var avatarUrl = path.join(config.root, config.avatarFolder);
  var thumbnail = req.body.imgdata.replace(config.baseImage, '');
  var format = config.imageFormat;

  fs.writeFile(tempUrl + req.body.userId + format,
    new Buffer(thumbnail, 'base64'),
    function(err) {
      if (err) return validationTrace(res, err, 'Error writing file on avatar method', 500);

      image.image(
        200,
        tempUrl + req.body.userId + format,
        avatarUrl + req.body.userId + format,
        function() {
          fs.unlink(tempUrl + req.body.userId + format, function(err) {
            if (err) return validationTrace(res, err, 'Error deleting the image on avatar method', 500);

            validationTrace(null, null, 'Success on avatar method', 200);
            return res.send(200);
          });
        },
        function(err) {
          return validationTrace(res, err, 'Error saving the image on avatar method', 500);
        }
      );
    }
  );
};

/**
 * Get my info
 */
exports.me = function(req, res) {
  return res.send(200, 'Api not ready');
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res) {
  res.redirect('/');
};
