'use strict';

var should = require('should');
var app = require('../../../app');
var request = require('supertest');
var User = require('.././user.model');

var user = new User({
  provider: 'local',
  name: 'Fake User',
  email: 'ochoaseguralex@gmail.com',
  password: 'password',
  username: 'yololo'
});

describe('User Controller', function() {

  beforeEach(function(done) {
    User.remove().exec().then(function() {
      done();
    });
  });

  //TODO: We need to have access to an unlimited time facebook token for test usages
  /*it('should save FACEBOOK user with avatar', function(done) {
    var httRequestObject = {
      access_token: 'CAAVkFXVLgh0BAJMWBfMHg1QMf02Jmjt4vd4kZBKybgm0yTZBA3VK1xmV3aEstHhChZABGFdZAOKGOucjbN22nI45nZCnqcwIPIZAd5IDvpXAnZBE1acEhFNlGq50sMRxqVgZCWk0FrQaeDraXlmALFRtayif81iHmCyDgOErqIjZBSuMnMiT71gF6B6ZBJveG74ZBKmAZC2yCPsXY9Fi73qhznVW'
    };

    request(app)
      .post('/api/users/facebook')
      .send(httRequestObject)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        should.exist(res.body);
        should.exist(res.body.avatar);
        should.exist(res.body._id);
        done();
      });
  });*/

  it('should save user from EMAIL and search in DB for the data', function(done) {

    //Timeout for sending email (sending email could be more than the default, 2 seconds)
    this.timeout(5000);

    var httRequestObject = {
      username: user.username,
      email: user.email,
      password: user.password
    };
    var httpParamObject = {
      id: ''
    };

    request(app)
      .post('/api/users')
      .send(httRequestObject)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        should.exist(res.body);
        should.exist(res.body._id);
        should.exist(res.body.hashedPassword);
        httpParamObject.id = res.body._id;

        request(app)
          .get('/api/users/' + httpParamObject.id)
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function(error, response) {
            if (error) return done(err);
            should.exist(response.body);
            should.not.exist(response.body._id);
            done();
          });
      });
  });

  it('should return a single user object by id', function(done) {

    //Timeout for sending email (sending email could be more than the default, 2 seconds)
    this.timeout(5000);

    var httRequestObject = {
      username: user.username,
      email: user.email,
      password: user.password
    };
    var httpParamObject = {
      id: ''
    };

    request(app)
      .post('/api/users')
      .send(httRequestObject)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        should.exist(res.body);
        should.exist(res.body._id);
        should.exist(res.body.hashedPassword);
        httpParamObject.id = res.body._id;

        request(app)
          .get('/api/users/' + httpParamObject.id)
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function(error, response) {
            if (error) return done(err);
            should.exist(response.body);
            should.not.exist(response.body._id);
            done();
          });
      });
  });

  it('should return a single user object using email/username and password', function(done) {

    //Timeout for sending email (sending email could be more than the default, 2 seconds)
    this.timeout(5000);

    var httRequestObject = {
      username: user.username,
      email: user.email,
      password: user.password
    };

    request(app)
      .post('/api/users')
      .send(httRequestObject)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        should.exist(res.body);
        should.exist(res.body._id);
        should.exist(res.body.hashedPassword);

        request(app)
          .get('/api/users/login/' + httRequestObject.email + '/' + httRequestObject.password)
          .send(httRequestObject)
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function(error, response) {
            if (error) return done(err);
            should.exist(response.body);
            should.exist(response.body.username);
            should.exist(response.body.email);
            should.exist(response.body._id);
            done();
          });
      });
  });
});
