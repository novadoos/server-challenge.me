'use strict';

var express = require('express');
var controller = require('./user.controller');
var router = express.Router();

router.get('/', controller.index);
//router.get('/me', controller.me); //TODO:Pending for web version
router.get('/lists/:username/:myUsername', controller.getUsersByUsername);
router.get('/login/:email/:password', controller.login);
router.get('/:id', controller.show);
router.get('/:id/followers', controller.getFollowers);
router.get('/:id/following/users', controller.getFollowingUsers);
router.get('/:id/following/challenges', controller.getFollowingChallenges);
// router.get('/facebook/:facebookToken', controller.validateFacebookToken);
router.post('/', controller.create);
router.post('/facebook', controller.createFromFacebook);
// router.post('/facebook/renewToken', controller.renewToken);
router.post('/avatar', controller.avatar);
router.post('/setUserFollower', controller.setUserFollower);
router.post('/setChallengeFollower', controller.setChallengeFollower);
router.put('/:id/password', controller.changePassword);
router.put('/:id', controller.disable);
router.delete('/:id', controller.destroy);

module.exports = router;
