'use strict';

var email = require('../../config/email');
var path = require('path');
var templatesDir   = path.resolve(__dirname, 'email-templates');
var config = require('../../config/environment');
var emailTemplates = require('email-templates');

var locals = {
  email: null,
  name: {
    first: null,
    id: null,
    url: null,
    pass: null
  },
  subject: null
};

//Send email to 1 user
function send(templateName, mailOptions, callback) {
  emailTemplates(templatesDir, function(err, template) {
    if (err) {
      callback(err);
    } else {
      template(templateName, locals, function(error, html) {
        if (error) {
          callback(error);
        } else {
          mailOptions.html = html;
          email.sendMail(mailOptions, callback);
        }
      });
    }
  });
}

exports.welcome = function(user, callback) {
  //set locals for template
  //TODO: Create WELCOME template with locals configuration

  //mail configuration
  var mailOptions = {
    from: config.email.from,
    to: user.email,
    subject: config.email.welcomeSubject
  };

  //send email
  send('welcome', mailOptions, callback);
};

exports.destroy = function(user, callback) {
  //set locals for template
  //TODO: Create DESTROY template with locals configuration

  //mail configuration
  var mailOptions = {
    from: config.email.from,
    to: user.email,
    subject: config.email.welcomeSubject
  };

  //send email
  send('destroy', mailOptions, callback);
};

exports.disable = function(user, callback) {
  //set locals for template
  //TODO: Create DISABLE template with locals configuration

  //mail configuration
  var mailOptions = {
    from: config.email.from,
    to: user.email,
    subject: config.email.welcomeSubject
  };

  //send email
  send('disable', mailOptions, callback);
};
