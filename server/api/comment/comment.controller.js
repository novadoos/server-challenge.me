'use strict';

var _ = require('lodash');
var Comment = require('./comment.model');
var Challenge = require('../challenge/challenge.model');
var Logger = require('../../service/logger');
var logger = Logger.createLog().child({component: 'comments'});

var validationTrace = function(res, err, message, status) {
  if (!err && message) err = new Error(message);
  if ([500, 408, 400].indexOf(status) > -1) {
    logger.error({err: err}, message);
    if (res && res.json) return res.json(status, err);
  } else if ([200, 201, 204].indexOf(status) > -1) {
    logger.info(message);
  } else if ([401].indexOf(status) > -1) {
    logger.warn(message);
    if (res && res.json) return res.json(status, err);
  }
};

var createCommentObject = function(obj, challengeId) {
  var comment = {
    id: obj._id ? obj._id : 0,
    created: obj.created ? obj.created : null,
    author: {
      id: obj.author.id ? obj.author.id : 0,
      username: obj.author.username ? obj.author.username : ''
    },
    comment: obj.comment ? obj.comment : ''
  };
  if (challengeId) comment.challengeId = challengeId;
  return comment;
};

// Get list of comments
exports.index = function(req, res) {
  //Comment.find(function(err, comments) {
  //  if (err) { return validationTrace(res, err, 'Error finding the comment on index method', 500); }
  //
  //  validationTrace(null, null, 'Success on index method', 200);
  //  return res.json(200, comments);
  //});
  return res.send(200, 'Api not usable');
};

// Get a single comment
exports.show = function(req, res) {
  //Comment.findById(req.params.id, function(err, comment) {
  //  if (err) { return validationTrace(res, err, 'Error finding the comment on show method', 500); }
  //  if (!comment) validationTrace(res, null, 'No comment found on show method', 404);
  //
  //  validationTrace(null, null, 'Success on index method', 200);
  //  return res.json(200, comment);
  //});
  return res.send(200, 'Api not usable');
};

// Get comments for a challenge
exports.showAll = function(req, res) {
  if (req.params.challengeId && req.params.skip) {
    var findQuery = {
      challengeId: req.params.challengeId,
      parentId: null
    };
    var finalComments = [];

    //TODO: This is not the best way to paginate. Improve!
    Comment.find(findQuery, {}, {skip: req.params.skip, limit: 10}, function(err, results) {
      if (err) return validationTrace(res, err, 'Error finding comment on showAll method', 500);

      function getReplies(i) {
        if (i < results.length) {
          var finalObject = createCommentObject(results[i], req.params.challengeId);

          findQuery.parentId = results[i]._id;
          Comment.find(findQuery, {}, {limit: 2}, function(childError, childResults) {
            if (childError) return validationTrace(res, childError, 'Error finding comments childs in showAll method', 500);

            finalObject.replies = [];

            function getReplyDetail(j) {
              if (j < childResults.length) {
                var childObject = createCommentObject(childResults[j]);
                finalObject.replies.push(childObject);
                getReplyDetail(j + 1);
              } else {
                finalComments.push(finalObject);
                getReplies(i + 1);
              }
            }
            getReplyDetail(0);
          });
        } else {
          validationTrace(null, null, 'Success on showAll method', 200);
          return res.json(200, finalComments);
        }
      }
      getReplies(0);
    });
  } else {
    return validationTrace(res, null, 'No data provided in showAll method', 400);
  }
};

// Get more replies for a comment in a challenge
exports.showMoreReplies = function(req, res) {
  if (req.params.challengeId && req.params.parentId && req.params.skip) {
    var findQuery = {
      challengeId: req.params.challengeId,
      parentId: req.params.parentId
    };
    var finalReplies = [];

    Comment.find(findQuery, {}, {skip: req.params.skip, limit: 10}, function(err, results) {
      if (err) return validationTrace(res, err, 'Error finding comment in showMoreReplies method', 500);
      for (var i = 0; i < results.length; i++) {
        finalReplies.push(createCommentObject(results[i]));
      }
      validationTrace(null, null, 'Success on showMoreReplies method', 200);
      return res.json(200, finalReplies);
    });
  } else {
    return validationTrace(res, null, 'No data provided in showMoreReplies challenges', 500);
  }
};

// Creates a new comment in the DB.
exports.create = function(req, res) {
  if (req.body.challengeId && req.body.commentMessage &&
    req.body.userId && req.body.userUsername) {
    try {
      var parentId = req.body.parentId ? req.body.parentId : null;

      var newComment = new Comment();
      newComment.challengeId = req.body.challengeId;
      newComment.created = new Date();
      newComment.author = {
        id: req.body.userId,
        username: req.body.userUsername
      };
      newComment.comment = req.body.commentMessage;

      if (parentId) newComment.parentId = parentId;

      newComment.save(function(err, comment) {
        if (err) { return validationTrace(res, err, 'Error saveing comment in create method', 500); }

        Challenge.findOne({_id: req.body.challengeId}, {_id:false, commentsCount:true}, function(error, challenge) {
          if (error) return validationTrace(res, error, 'Error finding challenge in create method', 500);
          if (!challenge) return validationTrace(res, null, 'No challenge found in create method', 404);

          var commentsCounter = challenge.commentsCount !== undefined &&
            challenge.commentsCount !== null  ? parseFloat(challenge.commentsCount) + 1 : 1;

          Challenge.update({_id: req.body.challengeId}, {$set: {commentsCount: commentsCounter}}, function(updErr, updChallenge) {
            if (updErr) return validationTrace(res, updErr, 'Error updating challenge on create method', 500);
            if (updChallenge) {
              validationTrace(null, null, 'Success on create method', 201);
              return res.json(201, comment);
            }
          });
        });
      });
    } catch (catchError) {
      return validationTrace(res, catchError, 'Error on create method', 500);
    }
  } else {
    return validationTrace(res, new Error('No data provided in create method'), 400);
  }
};

// Updates an existing comment in the DB.
exports.update = function(req, res) {
  //if (req.body._id) { delete req.body._id; }
  //Comment.findById(req.params.id, function(err, comment) {
  //  if (err) return validationTrace(res, err, 'Error finding comment on update method', 500);
  //  if (!comment) return validationTrace(res, null, 'No comment found on update method', 404);
  //
  //  var updated = _.merge(comment, req.body);
  //  updated.save(function(err) {
  //    if (err) { return validationTrace(res, err, 500); }
  //
  //    validationTrace(null, null, 'Success on update method', 200);
  //    return res.json(200, comment);
  //  });
  //});
  return res.send(200, 'Api not usable');
};

// Deletes a comment from the DB.
exports.destroy = function(req, res) {
  //Comment.findById(req.params.id, function(err, comment) {
  //  if (err) return validationTrace(res, err, 'Error finding comment in destroy method', 500);
  //  if (!comment) return res.send(404);
  //
  //  comment.remove(function(err) {
  //    if (err) return validationTrace(res, err, 'Error removing comment on destroy method', 500);
  //
  //    validationTrace(null, null, 'Success on destroy method', 204);
  //    return res.send(204);
  //  });
  //});
  return res.send(200, 'Api not usable');
};
