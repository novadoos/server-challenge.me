'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
  challengeId: Schema.Types.ObjectId,
  parentId: Schema.Types.ObjectId,
  created: Date,
  author: {
    id: Schema.Types.ObjectId,
    username: String
  },
  comment: String
});

module.exports = mongoose.model('Comment', CommentSchema);
