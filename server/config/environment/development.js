'use strict';

// Development specific configuration
// ==================================
module.exports = {
  ip: process.env.OPENSHIFT_NODEJS_IP || process.env.IP || undefined,

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/challengeme-dev'
  },

  errorCodes: {
    ETIMEDOUT: 'ETIMEDOUT'
  },

  providers:{
    local: 'local'
  },

  facebook: {
    clientID: '1517418208526877',
    clientSecret: '62e3a31d20983ce28a890c7d0d125b07'
  },

  google: {
    authUri: 'https://accounts.google.com/o/oauth2/auth',
    tokens: { //This tokens are provided by us with a diferent API
      access_token: 'ya29.5AHg1VZR5vYbVuTSkpIzxupGsqMHWddfuGrfIiooH3FCuPKY0wL4kjU3EgrQk9xMHjdH',
      token_type: 'Bearer',
      refresh_token: '1/5nZ1t6GlSQdA2gi5zzQ2-yGwPh-qaOloy6FVeU0y1LJIgOrJDtdun6zK6XiATCKT',
      expiry_date: 1441329822338
    },
    oauth2: {
      secret: '-ZiOCp9dY-8TQOYGrk554bTc',
      clientId: '316722061089-1qc9qt4oomjnkupuoi16vvd3ebk95ldk.apps.googleusercontent.com',
      redirectUri: 'http://localhost:9000/oauth2callback'
    }
  },

  roles:{
    user: 'user'
  },

  //Fills DB with dummy data
  seedDB: false
};
